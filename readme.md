This is a Python program that makes use of f strings so the minimum Python version is 3.6.

This sudoku_solver should solve most human solvable sudoku games. It implements a SudokuGame class and a Cell class.

The SudokuGame object takes in a gamestring of 81 characters, one for each cell on the board, and initializes a Cell object for each character.

Cell objects track their position on the gameboard, their value, their possible values, and the locations of their neighbors.

The SudokuGame class implements methods that attempt to fill in cell values based on their possible values and the possible values of their neighbors.

After filling in deducable values it attempts to make a guess on cells that have two possible values by initializing a test board and running through a fill cycle on that test board. If a guess results in an invalid board state (no possible values in a cell or two neighbor cells with the same value) the other option is the correct choice and it is used instead.

Core duduction methods are fill_possibles, find_singles, and find_pairs.


fill_possibles fills in any cell that only has one possible value.

find_singles looks at a set of neighbors in the same row, column, or grid and fills in a cell that is the only one that contains a given possible value.

find_pairs looks for pairs of cells in the same row, column, or grid that have only the same two possible values and removes those values from the possible values for all of the other cells in the same neighbor set.

After the three core deduction methods are run if the puzzle isn't solved then a guess is taken. The guess method loops through the cells that have only two possible values and instantiates a test game with the current game state and fills that cell value in with the first of the two possible values. It then runs the core deduction methods on the test board. If the choice results in an invalid state the test board is discarded and the other choice is filled in on the original board. A new solve cycle is run, up to and including a new guess so there is some recursion happening in the guess method. If the test board moves through a deduction cycle with no solution the test board is discarded and a guess is taken with the other option. If the test board results in a solved game the original gameboard is reinitialized with the solved gamestring and the solution is presented.

If the script is called as main it will loop through puzzle files stored in the subfolder puzzles. 480 test puzzles are included for proof of concept. These puzzles were taken from http://opensudoku.moire.org/#about-puzzles and are shared here with permission, but the author of OpenSudoku speculates the gamedata itself would be public domain.
